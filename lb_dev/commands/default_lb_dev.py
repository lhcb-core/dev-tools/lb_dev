from lb_dev_project.project.project import Project

def default_lb_dev(project, dev, dev_dir, nightly_base, nightly, help_nightly_local, user_area, no_user_area, siteroot, platform, force_platform, list, list_versions, list_platforms, name, dest_dir, git, with_fortran):
    metadata = {
            'project': project,
            'dev': dev,
            'dev_dir': dev_dir,
            'nightly_base': nightly_base,
            'nightly': nightly,
            'help_nightly_local': help_nightly_local,
            'user_area': user_area,
            'no_user_area': no_user_area,
            'siteroot': siteroot,
            'platform': platform,
            'force_platform': force_platform,
            'list': list,
            'list_versions': list_versions,
            'list_platforms': list_platforms,
            'name': name,
            'dest_dir': dest_dir,
            'git': git,
            'with_fortran': with_fortran
        }
    
    project = Project(metadata, True)
    
    project.new_project()