from lb_dev.commands.copyright.create_license_file.create_license_file import create_license_file_if_not_exists, create_license_file, replace_license_placeholders, get_license_text, write_license_to_file
from lb_utils.file_utils import FileUtils

from tests.temporary_directory_handler import TemporaryDirecotryHandler

from datetime import datetime
from os.path import basename
from os import mkdir
from os.path import exists
from shutil import rmtree, copy
import tempfile
from uuid import uuid1
from unittest.mock import Mock, patch
from requests.models import Response

def test_replace_mit_license_text():
    mock_license_text = 'mock license text [year] [fullname]'
    mock_license_content = replace_license_placeholders(mock_license_text, 'mit')

    assert 'mock license text {} {}'.format(str(datetime.now().year), 'CERN') == mock_license_content

def test_replace_apache2_license_text():
    mock_license_text = 'mock license text [yyyy] [name of copyright owner]'
    mock_license_content = replace_license_placeholders(mock_license_text, 'apache-2.0')

    assert 'mock license text {} {}'.format(str(datetime.now().year), 'CERN') == mock_license_content

def test_replace_gpl3_license_text():
    mock_license_text = 'mock license text END OF TERMS AND CONDITIONS'
    mock_license_content = replace_license_placeholders(mock_license_text, 'gpl-3.0')

    assert 'mock license text ' == mock_license_content

def test_replace_random_license_text():
    mock_license_text = 'mock license text [yyyy] [name of copyright owner]'
    mock_license_content = replace_license_placeholders(mock_license_text, 'random license')

    assert 1 == mock_license_content

@patch('lb_dev.commands.copyright.create_license_file.create_license_file.requests.get')
@patch('lb_dev.commands.copyright.create_license_file.create_license_file.Response.json')
def test_get_license_text(json_mocker, request_get_mocker):
    request_get_mocker.return_value = Response()

    json_mocker.return_value = mock_response()

    value = get_license_text('mit')
    assert value == 'some text'

@patch('lb_dev.commands.copyright.create_license_file.create_license_file.requests.get')
@patch('lb_dev.commands.copyright.create_license_file.create_license_file.Response.json')
def test_create_license_file_if_not_exists(json_mocker, request_get_mocker):
    request_get_mocker.return_value = Response()

    json_mocker.return_value = mock_response()
    
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        assert create_license_file_if_not_exists(folder_name, 'COPYING', 'mit') == 0

def test_create_license_file_already_exists():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = '/'.join([folder_name, 'LICENSE'])

        FileUtils.write_to_file(file_name, 'this is a license')
        
        result_code = create_license_file_if_not_exists(folder_name, 'COPYING', 'mit')

        assert result_code == 1

@patch('lb_dev.commands.copyright.create_license_file.create_license_file.requests.get')
@patch('lb_dev.commands.copyright.create_license_file.create_license_file.Response.json')
def test_create_license_file(json_mocker, request_get_mocker):
    request_get_mocker.return_value = Response()

    json_mocker.return_value = mock_response()

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        create_license_file(folder_name, 'COPYING', 'mit')

        assert exists("{}/COPYING".format(folder_name))

@patch('lb_dev.commands.copyright.create_license_file.create_license_file.requests.get')
@patch('lb_dev.commands.copyright.create_license_file.create_license_file.Response.json')
def test_create_license_file_bad_name(json_mocker, request_get_mocker):
    request_get_mocker.return_value = Response()

    json_mocker.return_value = mock_response()

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        assert create_license_file(folder_name, 'some random name', 'mit') == 1


def test_write_license_to_file():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        assert write_license_to_file('mit', '{}/LICENSE'.format(folder_name), 'this is a license file for [fullname] created in [year]') == 0

def test_write_license_to_file_bad_license_type():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        assert write_license_to_file('not mit', '{}/LICENSE'.format(folder_name), 'this is a license file for [fullname] created in [year]') == 1


def get_temp_file(path, content=None):
    temp_file1 = open(path, "w+")
    if content:
        temp_file1.writelines(content)
    temp_file1.close()

    return path

def mock_response(*args, **kwargs):
        return {'body': 'some text'}

