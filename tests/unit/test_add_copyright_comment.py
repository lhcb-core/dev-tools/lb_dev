from tests.temporary_directory_handler import TemporaryDirecotryHandler

from lb_dev.commands.copyright.add_copyright_comment.add_copyright_comment import get_copyright_offset, get_copyright_text, add_copyright_to_file, add_copyright, update_copyright_year
from lb_utils.file_utils import FileUtils
from lb_dev.constants import license_constants

from unittest.mock import patch

def test_get_copyright_offset():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name))   

        assert get_copyright_offset(['# copyright statement', 'rest of the script'], file_name1, 'py') == 0

def test_get_copyright_offset_with_encoding():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name))   

        assert get_copyright_offset(['# coding=utf8', 'rest of the script'], file_name1, 'py') == 1

def test_get_copyright_offset_with_shebang():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name))   

        assert get_copyright_offset(['#!/bin/python', 'rest of the script'], file_name1, 'py') == 1

def test_get_copyright_offset_with_shebang_and_encoding():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name))   

        assert get_copyright_offset(['#!/usr/bin/python', '# -*- encoding: utf8 -*-', 'rest of the script'], file_name1, 'py') == 2

def test_get_copyright_offset_xml():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.xml'.format(folder_name))   

        assert get_copyright_offset(['', 'rest of the script'], file_name1, 'xml') == 1

def test_get_copyright_offset_xml_ent():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.ent'.format(folder_name))   

        assert get_copyright_offset(['', 'rest of the script'], file_name1, 'xml') == 0

def test_get_copyright_offset_lcgdict():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.xml'.format(folder_name))   

        assert get_copyright_offset(['<lcgdict>', '[<selection>]', '<class [name="classname"] [pattern="wildname"]'], file_name1, 'xml') == 0

def test_get_copyright_offset_xml_with_comment():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.xml'.format(folder_name))   

        assert get_copyright_offset(['<!--Your comment-->'], file_name1, 'xml') == 0

def test_get_copyright_text():
    expected_result = '''
(c) Copyright 2020 myself

This software is distributed under the terms of the Apache License,
Version 2.0, copied verbatim in the file "LICENSE".
'''.strip()

    assert get_copyright_text(2020, 'myself', 'apache-2.0', 'LICENSE') == expected_result

def test_get_copyright_text_for_cern():
    expected_result = '''
(c) Copyright 2020 CERN

This software is distributed under the terms of the Apache License,
Version 2.0, copied verbatim in the file "LICENSE".

In applying this licence, CERN does not waive the privileges and immunities
granted to it by virtue of its status as an Intergovernmental Organization
or submit itself to any jurisdiction.
'''.strip()

    assert get_copyright_text(2020, 'CERN', 'apache-2.0', 'LICENSE') == expected_result

def test_add_copyright_to_file():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        
        assert not FileUtils.has_pattern(file_name1, license_constants.COPYRIGHT_SIGNATURE)

        add_copyright_to_file(file_name1, 2020, 'gpl-3.0', 'myself', 'LICENSE')

        assert FileUtils.has_pattern(file_name1, license_constants.COPYRIGHT_SIGNATURE)

def test_add_copyright():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        assert add_copyright([file_name1, file_name2, file_name3], 2020, 'gpl-3.0', 'myself', 'LICENSE', False) == 0

def test_add_copyright_unsupported_file():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name3 = get_temp_file('{}/some_file3.go'.format(folder_name), 'this is a script\nwith alotuva lines')

        assert add_copyright([file_name1, file_name2, file_name3], 2020, 'gpl-3.0', 'myself', 'LICENSE', False) == 1

def test_add_copyright_unsupported_file_force():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name3 = get_temp_file('{}/some_file3.go'.format(folder_name), 'this is a script\nwith alotuva lines')

        assert add_copyright([file_name1, file_name2, file_name3], 2020, 'gpl-3.0', 'myself', 'LICENSE', True) == 0

def test_add_copyright_already_has_copyright():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# already copyright statement\nthis is a script\nwith alotuva lines')
        file_name3 = get_temp_file('{}/some_file3.go'.format(folder_name), 'this is a script\nwith alotuva lines')

        assert add_copyright([file_name1, file_name2, file_name3], 2020, 'gpl-3.0', 'myself', 'LICENSE', True) == 1

def test_update_copyright_year():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name1], 2020, 'mit', 'myself', 'License', False)

        assert update_copyright_year([file_name1], '2008-2012') == 0

def get_temp_file(path, content=None):
    temp_file1 = open(path, "w+")
    if content:
        temp_file1.writelines(content)
    temp_file1.close()

    return path

   