from lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment import report, get_matching_files, get_potential_files, get_files, check_copyright, get_all_files_tracked_in_repo

from tests.temporary_directory_handler import TemporaryDirecotryHandler

from unittest.mock import Mock, patch

def test_report():
    expected_result = '''The following 3 files do not contain a copyright statement:
- file1
- file2
- file3

you can fix them with the command lb-dev add-copyright
'''

    assert report(['file1', 'file2', 'file3']) == expected_result

def test_report_inverted():
    expected_result = '''The following 3 files contain a copyright statement:
- file1
- file2
- file3'''

    assert report(['file1', 'file2', 'file3'], True) == expected_result

def test_report_target():
    expected_result = '''The following 3 files do not contain a copyright statement:
- file1
- file2
- file3
You can fix the 3 files without copyright statement with:

$ lb-check-copyright --porcelain some target | xargs -r lb-dev add-copyright
'''

    assert report(['file1', 'file2', 'file3'], False, 'some target') == expected_result

def test_report_inverted_target():
    expected_result = '''The following 3 files contain a copyright statement:
- file1
- file2
- file3'''

    assert report(['file1', 'file2', 'file3'], True, 'some target') == expected_result

def test_get_matching_files():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        expected_result = [file_name3]

        assert get_matching_files([file_name1, file_name2, file_name3], False) == expected_result 

def test_get_matching_files_inverted():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        expected_result = [file_name1, file_name2]

        assert get_matching_files([file_name1, file_name2, file_name3], True) == expected_result 

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_potential_files(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert get_potential_files(None, []).sort() == [file_name1, file_name2, file_name3].sort()

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_potential_files_with_empty_file(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name)) 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert get_potential_files(None, []).sort() == [file_name1, file_name2].sort()

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_potential_files_with_excluded_file(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.c'.format(folder_name)) 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert get_potential_files(None, [r'.+\.c$']).sort() == [file_name1, file_name2].sort()
        
@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_files(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.c'.format(folder_name)) 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert list(get_files()).sort() == [file_name1, file_name2, file_name3].sort()

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_files_with_folder_in_list(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 

        mocker.return_value = [file_name1, file_name2, folder_name]

        assert list(get_files()).sort() == [file_name1, file_name2].sort()

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_get_files_with_not_in_checked(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.go'.format(folder_name)) 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert list(get_files()).sort() == [file_name1, file_name2, file_name3].sort()

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_check_copyright(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), '# copyright statement\nrest of the script') 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert check_copyright('some reference', False, '\n', False, []) == 0

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_check_copyright_file_with_no_copyright(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert check_copyright('some reference', True, '\n', False, []) == 1

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_check_copyright_inverted(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), '# copyright statement\nrest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert check_copyright('some reference', False, '\n', True, []) == 1

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.get_all_files_tracked_in_repo')
def test_check_copyright_inverted_none_has_copyright(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'rest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'rest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        mocker.return_value = [file_name1, file_name2, file_name3]

        assert check_copyright('some reference', False, '\n', True, []) == 0

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.check_output')
def test_all_files_tracked_in_repo_no_reference(mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'rest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'rest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        mocker.return_value = '{}\x00{}\x00{}\x00'.format(file_name1, file_name2, file_name3).encode()

        assert list(get_all_files_tracked_in_repo(None)) == [file_name1, file_name2, file_name3]

@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.check_output')
@patch('lb_dev.commands.copyright.check_copyright_comment.check_copyright_comment.len')
def test_all_files_tracked_in_repo_with_reference(len_mocker, check_output_mocker):
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name1 = get_temp_file('{}/some_file1.py'.format(folder_name), 'rest of the script') 
        file_name2 = get_temp_file('{}/some_file2.py'.format(folder_name), 'rest of the script') 
        file_name3 = get_temp_file('{}/some_file3.py'.format(folder_name), 'rest of the script') 

        len_mocker.return_value = 0
        check_output_mocker.return_value = '{}\x00{}\x00{}\x00'.format(file_name1, file_name2, file_name3).encode()

        assert list(get_all_files_tracked_in_repo('some reference')) == [file_name1, file_name2, file_name3]

def get_temp_file(path, content=None):
    temp_file1 = open(path, "w+")
    if content:
        temp_file1.writelines(content)
    temp_file1.close()

    return path