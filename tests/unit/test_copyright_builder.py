from lb_dev.commands.copyright.builders.copyright_builder import CopyrightBuilder
from lb_dev.constants import license_constants

def test_add_year():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_year(1996)

    assert copyright_builder.year == 1996

def test_add_owner():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_owner('myself')

    assert copyright_builder.owner == 'myself' 

def test_add_license_type():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_type('mit')

    assert copyright_builder.license_type == 'mit' 

def test_add_license_file():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_file('LICENSE')

    assert copyright_builder.license_file == 'LICENSE' 

def test_get_license_text_mit():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_type('mit')

    assert copyright_builder.get_license() == license_constants.mit_license

def test_get_license_text_apache2():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_type('apache-2.0')

    assert copyright_builder.get_license() == license_constants.apache2_license

def test_get_license_text_gpl():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_type('gpl-3.0')

    assert copyright_builder.get_license() == license_constants.gpl3_license

def test_get_license_text_random_license():
    copyright_builder = CopyrightBuilder()
    copyright_builder.add_license_type('bad license')

    assert copyright_builder.get_license() == '[invalid license] bad license'

def test_build_copyright_default():
    expected_result = '''
(c) Copyright {year} {owner}

This software is distributed under the terms of the [invalid license] {license_type}, copied verbatim in the file "{license_file}".
'''.strip()

    copyright_builder = CopyrightBuilder()

    print(expected_result)
    print(copyright_builder.build_copyright())

    assert copyright_builder.build_copyright() == expected_result

def test_build_copyright():
    expected_result = '''
(c) Copyright 2020 myself

This software is distributed under the terms of the Apache License,
Version 2.0, copied verbatim in the file "LICENSE".
'''.strip()

    copyright_builder = CopyrightBuilder()
    copyright_builder.add_year(2020)
    copyright_builder.add_owner('myself')
    copyright_builder.add_license_type('apache-2.0')
    copyright_builder.add_license_file('LICENSE')

    assert copyright_builder.build_copyright() == expected_result

def test_build_copyright_for_cern():
    expected_result = '''
(c) Copyright 2020 CERN

This software is distributed under the terms of the Apache License,
Version 2.0, copied verbatim in the file "LICENSE".

In applying this licence, CERN does not waive the privileges and immunities
granted to it by virtue of its status as an Intergovernmental Organization
or submit itself to any jurisdiction.
'''.strip()

    copyright_builder = CopyrightBuilder()
    copyright_builder.add_year(2020)
    copyright_builder.add_owner('CERN')
    copyright_builder.add_license_type('apache-2.0')
    copyright_builder.add_license_file('LICENSE')

    assert copyright_builder.build_copyright() == expected_result

def test_build_copyright_with_cern_in_owner_but_not_for_cern():
    expected_result = '''
(c) Copyright 2020 someone with concerned about his right, coming from CERNobyl

This software is distributed under the terms of the Apache License,
Version 2.0, copied verbatim in the file "LICENSE".
'''.strip()

    copyright_builder = CopyrightBuilder()
    copyright_builder.add_year(2020)
    copyright_builder.add_owner('someone with concerned about his right, coming from CERNobyl')
    copyright_builder.add_license_type('apache-2.0')
    copyright_builder.add_license_file('LICENSE')

    print(copyright_builder.build_copyright())

    assert copyright_builder.build_copyright() == expected_result