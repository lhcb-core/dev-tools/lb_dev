from tests.temporary_directory_handler import TemporaryDirecotryHandler
from lb_dev.commands.copyright.add_copyright_comment.add_copyright_comment import add_copyright
from lb_dev.commands.copyright.scanner.copyright_scanner import CopyrightScanner

from pytest import raises

def test_extract_copyright():
    expected_result_text = '''###############################################################################
# (c) Copyright 2020 myself                                                   #
#                                                                             #
# This software is distributed under the terms of the MIT                     #
# License, copied verbatim in the file "License".                             #
###############################################################################'''

    expected_result = expected_result_text.split('\n')
    for i, line in enumerate(expected_result):
        expected_result[i] = line + '\n'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        copyright_statement, _, _ = copyright_scanner.extract_copyright_statement()

        assert copyright_statement == expected_result

def test_extract_c_copyright():
    expected_result_text = '''/*****************************************************************************\\
* (c) Copyright 2020 myself                                                   *
*                                                                             *
* This software is distributed under the terms of the MIT                     *
* License, copied verbatim in the file "License".                             *
\*****************************************************************************/'''

    expected_result = expected_result_text.split('\n')
    for i, line in enumerate(expected_result):
        expected_result[i] = line + '\n'

    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.c'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        copyright_statement, _, _ = copyright_scanner.extract_copyright_statement()
        print(copyright_statement)
        print(expected_result)
        assert copyright_statement == expected_result
        
def test_extract_owner():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        owner = copyright_scanner.extract_owner()

        assert owner == 'myself'

def test_extract_license_type():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        license_type = copyright_scanner.extract_license_type()

        assert license_type == 'mit'

def test_extract_license_file():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        license_file = copyright_scanner.extract_license_file_name()

        assert license_file == 'License'

def test_extract_year():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'mit', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        year = copyright_scanner.extract_copyright_year()

        assert year == '2020'

def test_extract_nonexistent_copyright():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)
        copyright_statement, start_line, end_line = copyright_scanner.extract_copyright_statement()

        assert copyright_statement == []
        assert start_line == 0
        assert end_line == 0

def test_extract_year_without_copyright_raises_exception():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)

        with raises(ValueError):
            assert copyright_scanner.extract_copyright_year()

def test_extract_owner_without_copyright_raises_exception():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)

        with raises(ValueError):
            assert copyright_scanner.extract_owner()

def test_extract_license_type_without_copyright_raises_exception():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)

        with raises(ValueError):
            assert copyright_scanner.extract_license_type()

def test_extract_license_name_without_copyright_raises_exception():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)

        with raises(ValueError):
            assert copyright_scanner.extract_license_file_name()

def test_mit_extract_license_from_line():
    copyright_scanner = CopyrightScanner()
    line = 'this is copyright contains an MIT License statement'

    assert copyright_scanner.extract_license_from_line(line) == 'mit'

def test_apache_extract_license_from_line():
    copyright_scanner = CopyrightScanner()
    line = 'this is copyright contains an Apache License statement'

    assert copyright_scanner.extract_license_from_line(line) == 'apache-2.0'

def test_gnu_extract_license_from_line():
    copyright_scanner = CopyrightScanner()
    line = 'this is copyright contains a GNU License statement'

    assert copyright_scanner.extract_license_from_line(line) == 'gpl-3.0'

def test_random_extract_license_from_line():
    copyright_scanner = CopyrightScanner()
    line = 'this is copyright contains a Random License statement'

    assert copyright_scanner.extract_license_from_line(line) == ''

def test_extract_random_license_raises_error():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        file_name = get_temp_file('{}/some_file1.py'.format(folder_name), 'this is a script\nwith alotuva lines')
        add_copyright([file_name], 2020, 'random_license', 'myself', 'License', False)

        copyright_scanner = CopyrightScanner()
        copyright_scanner.set_file_path(file_name)

        with raises(ValueError):
            assert copyright_scanner.extract_license_type()
    

def get_temp_file(path, content=None):
    temp_file1 = open(path, "w+")
    if content:
        temp_file1.writelines(content)
    temp_file1.close()

    return path