from lb_dev.commands.copyright.check_for_license_file.check_for_license import check_for_license
from tests.temporary_directory_handler import TemporaryDirecotryHandler

from os.path import basename
from os import mkdir
from os.path import exists
from shutil import rmtree, copy
import tempfile
from uuid import uuid1


def test_check_for_license_file():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        temp_file = open("{}/LICENSE".format(folder_name), "w+")
        temp_file.write("some string")
        temp_file.close()

        # pre-commit hooks need to exit with 0 value if everything worked as it should 
        # and non-zero value if something went wrong
        assert check_for_license('{}'.format(folder_name)) == 0


def test_check_for_license_file_fail():
    with TemporaryDirecotryHandler.create_temp_directory() as folder_name:
        temp_file = open("./{}/random_file_name.rst".format(folder_name), "w+")
        temp_file.write("some string")
        temp_file.close()

        # pre-commit hooks need to exit with 0 value if everything worked as it should 
        # and non-zero value if something went wrong
        assert check_for_license('./{}'.format(folder_name)) != 0
